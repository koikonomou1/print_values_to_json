import json
import sys

if sys.version_info[0] >= 3:
    unicode = str

json_data = '{"x": 7}'

def _decode(o):
    # Note the "unicode" part is only for python2
    if isinstance(o, str) or isinstance(o, unicode):
        try:
            return int(o)
        except ValueError:
            return o
    elif isinstance(o, dict):
        return {k: _decode(v) for k, v in o.items()}
    elif isinstance(o, list):
        return [_decode(v) for v in o]
    else:
        return o


json_object = json.loads(json_data, object_hook=_decode) 

with open('values.json', 'w') as jsonfile:
	json.dump(json_object, jsonfile)

# Indent = spaces
print(json.dumps(json_object, indent = 1)) 
